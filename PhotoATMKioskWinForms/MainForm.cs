﻿using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json.Linq;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using PhotoATMKiosk;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using NLog;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;

namespace PhotoATMKioskWinForms
{
    public partial class MainForm : Form
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        DispatcherTimer mIdle;
        TransactionService _transactionService;

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);
        private string _machineId;
        private string _baseUrl;
        private string _startPage;
        private string _currentPage;
        private string _acroPath;
        private string _printerName;
        private string _pdfPath;
        protected ChromiumWebBrowser Browser;
        private const int INTERNET_OPTION_END_BROWSER_SESSION = 42;
        Timer timer1 = new Timer();
        Stopwatch noInputTimer = Stopwatch.StartNew();

        public MainForm()
        {
            string[] args = Environment.GetCommandLineArgs();
            string credentialEndpoint = null;
            string transactionEndpoint = null;
            string reportEndpoint = null;
            string pdfEndpoint = null;
            string acroPath = null;
            string printer = null;

            _machineId = args[1];
            _baseUrl = args[2];
            credentialEndpoint = args.Length > 3 ? args[3] : null;
            transactionEndpoint = args.Length > 4 ? args[4] : null;
            reportEndpoint = args.Length > 5 ? args[5] : null;
            pdfEndpoint = args.Length > 6 ? args[6] : null;
            acroPath = args.Length > 7 ? args[7] : null;
            printer = args.Length > 8 ? args[8] : "DS-RX1";
            _pdfPath = args.Length > 9 ? args[9] : @"C:\tmp\";
            _acroPath = acroPath;
            _printerName = printer;
            _startPage = _baseUrl + "/root/" + _machineId + "/en/view";

            _transactionService = new TransactionService(_machineId, _baseUrl, credentialEndpoint, transactionEndpoint, reportEndpoint, pdfEndpoint);

            InitializeComponent();
            InitializeChromium();

            timer1.Tick += new System.EventHandler(this.timer1_Tick);
            timer1.Interval = 1000;
            timer1.Enabled = false;

        }


        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            settings.CefCommandLineArgs.Add("disable-gpu", "1");
            settings.CefCommandLineArgs.Add("disable-gpu-vsync", "1");
            settings.CefCommandLineArgs.Add("touch_event", "0");
            settings.IgnoreCertificateErrors = true;
            settings.LogSeverity = LogSeverity.Verbose;
            settings.MultiThreadedMessageLoop = true;

            Cef.Initialize(settings);
            Browser = new ChromiumWebBrowser(_startPage);
            Browser.Name = "Browser";
            Browser.TabIndex = 0;
            Browser.FrameLoadStart += RevealLoader;
            Browser.FrameLoadEnd += ChangedView;
            Browser.MouseClick += BrowserOnMouseClick;
            Browser.MouseMove += BrowserOnMouseMove;
            Browser.Click += BrowserOnClick;
            this.Controls.Add(Browser);

        }

        private void BrowserOnClick(object sender, EventArgs eventArgs)
        {
            noInputTimer.Restart();

        }

        private void BrowserOnMouseMove(object sender, MouseEventArgs mouseEventArgs)
        {
            noInputTimer.Restart();

        }

        private void BrowserOnMouseClick(object sender, MouseEventArgs mouseEventArgs)
        {
            noInputTimer.Restart();

        }

        void timer1_Tick(object sender, EventArgs e)
        {
            if (_currentPage != _startPage && noInputTimer.ElapsedMilliseconds > 180000)
                EndSession();
        }


        private void EndSession()
        {
            Browser.Load(_startPage);
            System.Threading.Thread.Sleep(500);
            InternetSetOption(IntPtr.Zero, INTERNET_OPTION_END_BROWSER_SESSION, IntPtr.Zero, 0);
        }

        public void ChangedView(object sender, FrameLoadEndEventArgs e)
        {

            _currentPage = e.Frame.Url;
            if (_currentPage.EndsWith("/cart/print/2"))
            {
                PrintMedia();
            }
            else if (_currentPage.Contains("www.instagram.com"))
            {
                ModifyInstagram();
            }

        }

        public void RevealLoader(object sender, FrameLoadStartEventArgs e)
        {
            noInputTimer.Restart();

            //Browser.ExecuteScriptAsync("$('.ajax-loader').fadeIn(250);");
        }

        public void ModifyInstagram()
        {
            Browser.ExecuteScriptAsync("function editInstagram() {$('a').remove();$('input[type=\"text\"],input[type=\"password\"]').addClass('js-entry');$('input').data('mode','instagram');var head = document.getElementsByTagName('head')[0];var callback = function () {script = document.createElement('script');script.type = 'text/javascript';script.src = '" + _baseUrl + "/dist/js/keyboard.js';head.appendChild(script);script = document.createElement('link');script.rel = 'stylesheet';script.href = '" + _baseUrl + "/dist/lib/virtual-keyboard/css/keyboard-basic.min.css';head.appendChild(script);script = document.createElement('link');script.rel = 'stylesheet';script.href = '" + _baseUrl + "/dist/css/keyboard.css';head.appendChild(script);};var script = document.createElement('script');script.type = 'text/javascript';script.src = '" + _baseUrl + "/dist/lib/virtual-keyboard/js/jquery.keyboard.js';script.onreadystatechange = callback;script.onload = callback;head.appendChild(script);};var modules_loaded=[];var callback=editInstagram;var head = document.getElementsByTagName('head')[0];var script = document.createElement('script');script.type = 'text/javascript';script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js';script.onreadystatechange = callback;script.onload = callback;head.appendChild(script);");
        }

        public void PrintMedia()
        {
            int mediaIndex = 0;
            string filename;
            PdfDocument document;
            JArray cartData;
            JObject currentTransaction;
            XTextFormatter tf = null;
            PdfPage page = null;
            XGraphics gfx = null;
            string transactionId = "";

            document = new PdfDocument();


            try
            {
                currentTransaction = _transactionService.FetchCurrentTransaction().Result;
                transactionId = currentTransaction["data"]["Transaction"]["id"].ToString();

                document.Info.Title = "Transaction " + transactionId;
                if (currentTransaction["data"]["Transaction"]["cart_data"] != null)
                    cartData = JArray.Parse(currentTransaction["data"]["Transaction"]["cart_data"].ToString());
                else
                    throw new Exception("Nothing to print");

                if (cartData.Count > 0)
                {
                    _transactionService.SetPrintSuccess(transactionId);

                    foreach (JObject media in cartData)
                    {
                        int rotation = Int32.Parse(media["rotation"].ToString());
                        int orientation = 0;
                        string imageUri = media["image"].ToString();
                        string orientationString = media["orientation"].ToString();
                        string location = null;
                        string gps = media["data"]["gps"].ToString().Trim();

                        if (gps != String.Empty)
                        {
                            location = JObject.Parse(gps)["name"].ToString();
                        }
                        JObject data = JObject.Parse(media["data"].ToString());
                        DateTimeOffset formattedDate;
                        string date = data["date"].ToString();
                        string caption = data["caption"].ToString();
                        if (date.Trim() != "")
                        {
                            formattedDate = DateTimeOffset.FromUnixTimeSeconds(long.Parse(date));
                            date = formattedDate.ToString("MM.dd.yyyy");
                        }

                        switch (orientationString)
                        {
                            case "portrait-notext":
                                orientation = 1;
                                break;
                            case "landscape-text":
                                orientation = 2;
                                break;
                            default:
                                orientation = 0;
                                break;
                        }

                        int column = mediaIndex % 2;
                        if (column == 0)
                        {
                            page = document.AddPage();
                            page.Width = XUnit.FromMillimeter(152);
                            page.Height = XUnit.FromMillimeter(102);
                            gfx = XGraphics.FromPdfPage(page);
                            tf = new XTextFormatter(gfx);
                        }

                        PlaceImage(gfx
                            , imageUri
                            , rotation
                            , orientation
                            , column
                        );

                        if (orientation != 1)
                            DrawCaptions(tf, gfx, orientation, column, date, caption, location, rotation);

                        mediaIndex++;
                    }

                    filename = _pdfPath + "\\" + transactionId + ".pdf";
                    filename = filename.Replace("\"", "");

                    logger.Info($"Trying to save file {filename}");
                    document.Save(filename);

                    AdobePrint PrintPDF = new AdobePrint(filename, _acroPath, _printerName);
                }
            }
            catch (Exception ex)
            {
                _transactionService.SetPrinterError(ex, transactionId, _machineId);
                _transactionService.SendErrorReport(ex, transactionId, _machineId);
                Browser.Load(_startPage);
            }
        }

        void PlaceImage(XGraphics gfx, string fileUri, int rotation, int mode, int col)
        {
            double[] rect = new double[] { 0, 0, 0, 0 };
            switch (mode)
            {
                case 0:
                    rect = new double[] { 3, 3, 70, 70 };
                    break;

                case 1:
                    rect = new double[] { 3, 3, 70, 95 };
                    break;

                case 2:
                    rect = new double[] { 18, 3, 55, 95 };
                    rotation++;
                    rotation = rotation > 3 ? 0 : rotation;
                    break;
            };

            if (col == 1)
                rect[0] += 76;

            DrawImage(
                gfx
                , fileUri
                , rotation
                , XUnit.FromMillimeter(rect[0])
                , XUnit.FromMillimeter(rect[1])
                , XUnit.FromMillimeter(rect[2])
                , XUnit.FromMillimeter(rect[3])
            );
        }

        void DrawCaptions(XTextFormatter tf, XGraphics gfx, int mode, int col, string dateText, string captionUri, string gpsText, int rotation)
        {
            XPdfFontOptions options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
            XFont font = new XFont("Lato", 10.0, XFontStyle.Regular, options);
            XGraphicsState state = gfx.Save();
            double[] daterect = new double[] { 0, 0, 0, 0 };
            double[] captionrect = new double[] { 0, 0, 0, 0 };
            double[] georect = new double[] { 0, 0, 0, 0 };

            switch (mode)
            {
                case 0:
                    daterect = new double[] { 3, 76.7, 70, 6.7 };
                    captionrect = new double[] { 3, 80.4, 70, 19.53 };
                    georect = new double[] { 27, 76.7, 20, 6.7 };

                    if (col == 1)
                    {
                        daterect[0] += 76;
                        captionrect[0] += 76;
                        georect[0] += 76;
                    }

                    break;

                case 1:
                    daterect = new double[] { 3, 76.7, 70, 6.7 };
                    captionrect = new double[] { 3, 80.4, 70, 19.53 };
                    georect = new double[] { 27, 76.7, 44, 6.7 };

                    if (col == 1)
                    {
                        daterect[0] += 76;
                        captionrect[0] += 76;
                        georect[0] += 76;
                    }

                    break;

                case 2:
                    daterect = new double[] { 15.3, 15, 70, 3.7 };
                    captionrect = new double[] { 15.3, 18.7, 70, 7.6 };
                    georect = new double[] { 41.3, 15, 44, 3.7 };

                    if (col == 1)
                    {
                        daterect[0] += 76;
                        captionrect[0] += 76;
                        georect[0] += 76;
                        gfx.RotateAtTransform(90, new XPoint(XUnit.FromMillimeter(97.3), XUnit.FromMillimeter(9.5)));
                    }
                    else
                        gfx.RotateAtTransform(90, new XPoint(XUnit.FromMillimeter(21.3), XUnit.FromMillimeter(9.5)));
                    break;
            };

            XRect date = new XRect(
                XUnit.FromMillimeter(daterect[0])
                , XUnit.FromMillimeter(daterect[1])
                , XUnit.FromMillimeter(daterect[2])
                , XUnit.FromMillimeter(daterect[3])
            );

            XRect geo = new XRect(
                XUnit.FromMillimeter(georect[0])
                , XUnit.FromMillimeter(georect[1])
                , XUnit.FromMillimeter(georect[2])
                , XUnit.FromMillimeter(georect[3])
            );

            tf.DrawString(
                dateText
                , font
                , new XSolidBrush(new XColor { R = 51, G = 122, B = 182 })
                , date
                , XStringFormats.TopLeft
            );
            try
            {
                DrawImage(
                    gfx
                    , captionUri
                    , rotation
                    , XUnit.FromMillimeter(captionrect[0])
                    , XUnit.FromMillimeter(captionrect[1])
                    , XUnit.FromMillimeter(captionrect[2])
                    , XUnit.FromMillimeter(captionrect[3])
                    , true
                );
            }
            catch (Exception ex)
            {
                logger.Error($"Error getting image {captionUri}: " + ex.Message);
            }

            if (gpsText != null && gpsText.Trim() != "")
            {
                double markerX = 23;
                double markerY = 75.3;
                if (mode == 2)
                {
                    markerX = 35.3;
                    markerY = 15;
                }

                if (col == 1)
                    markerX += 76;

                DrawImage(gfx, _baseUrl + "/dist/img/marker.png", 0, XUnit.FromMillimeter(markerX), XUnit.FromMillimeter(markerY), 11, 14);
                tf.DrawString(
                    gpsText
                    , font
                    , new XSolidBrush(new XColor { R = 0, G = 0, B = 0 })
                    , geo
                    , XStringFormats.TopLeft
                );
            }


            gfx.Restore(state);
        }

        void DrawImage(XGraphics gfx, string fileUri, int rotation, XUnit x, XUnit y, XUnit width, XUnit height, bool useDimensions = false)
        {
            try
            {
                Image photo = Image.FromStream(DownloadImage(fileUri));

                switch (rotation)
                {
                    case 1:
                        photo.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                    case 2:
                        photo.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        break;
                    case 3:
                        photo.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;
                };

                XImage image = XImage.FromGdiPlusImage(photo);

                if (useDimensions)
                {
                    width = image.PixelWidth * .75;
                    height = image.PixelHeight * .75;
                }

                gfx.DrawImage(image, x, y, width, height);
            }
            catch (Exception e)
            {
                logger.Error($"Error drawing image {fileUri} : {e.ToString()}");
            }
        }

        MemoryStream DownloadImage(string fileUri)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var task = httpClient.GetByteArrayAsync(new Uri(fileUri));
                task.Wait();
                if (task.IsFaulted)
                    throw new Exception($"Could not download {fileUri} : {task.Exception?.ToString()}");
                byte[] data = task.Result;
                MemoryStream ms = new MemoryStream(data, 0, data.Length);
                ms.Write(data, 0, data.Length);
                return ms;
            }
        }


        private Image DrawText(String text, Font font, Color textColor, Color backColor)
        {
            //first, create a dummy bitmap just to get a graphics object
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);

            //measure the string to see how big the image needs to be
            SizeF textSize = drawing.MeasureString(text, font);

            //free up the dummy image and old graphics object
            img.Dispose();
            drawing.Dispose();

            //create a new image of the right size
            img = new Bitmap((int)textSize.Width, (int)textSize.Height);

            drawing = Graphics.FromImage(img);

            //paint the background
            drawing.Clear(backColor);

            //create a brush for the text
            Brush textBrush = new SolidBrush(textColor);

            drawing.DrawString(text, font, textBrush, 0, 0);

            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            return img;

        }
    }

}
