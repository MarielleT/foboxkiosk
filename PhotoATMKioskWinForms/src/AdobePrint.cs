﻿using PdfSharp.Pdf.Printing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoATMKiosk
{
    public class AdobePrint
    {
        public AdobePrint(string file, string acroPath, string printerName)
        {
            PdfFilePrinter.AdobeReaderPath = acroPath;
            PdfFilePrinter printer = new PdfFilePrinter(file, printerName);

            printer.Print();
        }
    }
}