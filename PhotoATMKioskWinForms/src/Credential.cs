﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoATMKioskWinForms
{
    public class Credential
    {
        public string server;
        public string database;
        public string username;
        public string password;
    }
}