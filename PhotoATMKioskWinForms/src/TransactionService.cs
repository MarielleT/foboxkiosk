﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net.Http;
using CYINT.NETWorkServices;
using Newtonsoft.Json.Linq;
using NLog;

namespace PhotoATMKioskWinForms
{
    public class TransactionService
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        private string _machineId;
        private string _credentialEndpoint;
        private string _transactionEndpoint;
        private string _reportEndpoint;
        private string _pdfEndpoint;
        private string _connectionString;
        private string _baseUrl;
        private string _authKey;
        private MySqlConnection _Connection;


        public TransactionService(string machineId, string baseUrl = "https://photoatm.cyint.technology", string credentialEndpoint = "/credentials", string transactionPollEndpoint = "/app/en/transaction", string reportEndpoint = "/app/en/error/report", string pdfEndpoint = "/app/en/create/pdf")
        {
            _authKey = "lIBIliNErOCRAGireATerCHIBLEgiNeLuNtricKIrEpiCALiTyrueN";
            _baseUrl = baseUrl;
            _machineId = machineId;
            _credentialEndpoint = credentialEndpoint;
            _transactionEndpoint = transactionPollEndpoint;
            _reportEndpoint = reportEndpoint;
            _pdfEndpoint = pdfEndpoint;
            _connectionString = null;
            CallRetrieveCredentials();
        }

        async void CallRetrieveCredentials()
        {
            bool complete = await RetrieveCredentials();
        }

        public async Task<bool> RetrieveCredentials()
        {
            string results;
            string credentials;
            Credential MySqlCredentials;
            JObject jsonResults;
            NetworkService KioskNetwork = new NetworkService(_baseUrl);
            results = await KioskNetwork.FetchData(_credentialEndpoint, true, "authentication=" + _authKey);
            jsonResults = JObject.Parse(results);
            credentials = jsonResults["data"]["credentials"].ToString();
            MySqlCredentials = JsonConvert.DeserializeObject<Credential>(credentials);
            _connectionString = "SERVER=" + MySqlCredentials.server + ";DATABASE=" + MySqlCredentials.database + ";UID=" + MySqlCredentials.username + ";PASSWORD=" + MySqlCredentials.password + ";Allow User Variables=true";
            return true;
        }

        public async Task<JObject> FetchCurrentTransaction()
        {
            string paramVals = string.Concat("authentication=", this._authKey, "&machine=", this._machineId);

            logger.Info($"Start fetching transaction, params {paramVals}");
            string str = await (new NetworkService(this._baseUrl)).FetchData(this._transactionEndpoint, true, paramVals, null, "application/x-www-form-urlencoded");
            logger.Info($"Return = {str}");

            return JObject.Parse(str);
        }

        public async Task<JObject> FetchPDFData(string transactionID)
        {
            string results;
            NetworkService KioskNetwork = new NetworkService(_baseUrl);
            results = await KioskNetwork.FetchData(_pdfEndpoint + "/" + transactionID);
            return JObject.Parse(results);
        }

        public void SetPrintSuccess(string transactionId)
        {
            using (_Connection = new MySqlConnection(_connectionString))
            {
                _Connection.Open();
                MySqlCommand Command = _Connection.CreateCommand();
                Command.CommandText = "Update transaction set transaction_status=5 where id=?id";
                Command.Parameters.AddWithValue("?id", transactionId);
                Command.ExecuteNonQuery();
                Command = _Connection.CreateCommand();
                _Connection.Close();
            };
        }


        public void SetPrinterError(Exception ex, string transactionId, string machineId)
        {
            using (_Connection = new MySqlConnection(_connectionString))
            {
                _Connection.Open();
                MySqlCommand Command = _Connection.CreateCommand();
                Command.CommandText = "Update transaction set transaction_status=3, notes=CONCAT(notes,'...',?error) where id=?id";
                Command.Parameters.AddWithValue("?id", transactionId);
                Command.Parameters.AddWithValue("?error", ex.Message + " " + ex.Source + " " + ex.StackTrace + " " + ex.InnerException);
                Command.ExecuteNonQuery();
                Command = _Connection.CreateCommand();
                Command.CommandText = "Update machine set status='[\"5\"]', notes=CONCAT(notes,'...',?error) where id=?id";
                Command.Parameters.AddWithValue("?id", machineId);
                Command.Parameters.AddWithValue("?error", ex.Message + " " + ex.Source + " " + ex.StackTrace + " " + ex.InnerException);
                Command.ExecuteNonQuery();
                _Connection.Close();
            };
        }

        public async void SendErrorReport(Exception ex, string transactionId, string machineId)
        {
            string results;
            NetworkService KioskNetwork = new NetworkService(_baseUrl);
            results = await KioskNetwork.FetchData(_reportEndpoint, true, "authentication=" + _authKey + "&machine=" + _machineId + "&transaction=" + transactionId + "&error" + ex.Message);
        }
    }
}